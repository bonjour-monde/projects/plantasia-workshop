**P L A N T A S I A**<br>
: :  LISAA Graphisme<br>
: :  19-23 septembre 2022<br>

————————————<br>
L'objectif de cet atelier est d'approcher le dessin de caractère à travers la conception d'une super-famille typographique dont chacune des sous-familles sera inspirée par une plante. Le travail se fera en groupes de trois, chaque étudiant.e étant en charge de produire une déclinaison selon le modèle classique Romain-Italique-Gras.<br>
Les inspirations donneront lieu à une collecte de mots et d'images, suivuie d'une recherche calligraphique, permettant une meilleure compréhension et conceptualisation. Les résultats pourront au choix être illustratifs ou distanciés.  


————————————<br>
**C A L E N D R I E R**<br><br>
• **Lundi 19 septembre**<br>
    - Lancement/introduction du sujet<br>
    - Choix des “éléments” d'inspiration<br>
    - Collecte d'images et de mots<br>
    -- Pause déjeuner<br>
    - Recherche de rythmes graphiques et calligraphiques<br>
    - Introduction au logiciel Glyphs et à la notion de composantes<br>
    <br>
• **Mardi 20 septembre**<br>
    - Scan de formes graphiques produites<br>
    -- Pause déjeuner<br>
    - Travail de dessin sur Glyphs<br>
    - Suivi individuel<br>
    <br>
• **Mercredi 21 septembre**<br>
    - Poursuite du travail sur Glyphs<br>
    - Suivi individuel<br>
    <br>
• **Jeudi 22 septembre**<br>
    - Poursuite du travail sur Glyphs<br>
    - Suivi individuel<br>
    <br>
• **Vendredi 23 septembre**<br>
    - Fin du travail sur Glyphs<br>
    - Exports des fichiers typographiques<br>
    - Mise en page de visuels ”spécimen” (12 par groupes, noir&blanc, 1200x1200px, PNG)<br>
    -- Pause déjeuner<br>
    - Export des visuels “spécimen”<br>
    - Intégration des visuels dans la page web<br>
    - Restitution en commun<br>



————————————<br>
**L I E N S**

• [Télécharger le logiciel Glyphs](https://glyphsapp.com/buy)<br>
• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Oh No Type Co. blog](https://ohnotype.co/blog/tagged/teaching)<br>
• [Vieux spécimens typographiques numérisés (rassemblés par Emmanuel Besse)](https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc)<br>
• [Type Review Journal](https://fontreviewjournal.com/)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Font of the Month Club](http://fontofthemonth.club/)<br>
• [Stratégies italiques](http://strategiesitaliques.fr/)<br>
• [Guide pour le spacing, Fontsmith](https://www.fontsmith.com/blog/2018/02/05/how-to-space-a-typeface)<br>
• [Guides pour le spacing, Society of Fonts](https://www.societyoffonts.com/2018/09/26/spacing-a-font-part-2/)<br>
• [Article sur le spacing, OHno](https://ohnotype.co/blog/spacing)<br>

————————————<br>
**R É F É R E N C E S**

MODULES<br>
• [MODULES > Capitales (Edward Catich, ‘The Origin of the Serif’)](https://pampatype.com/thumbs/blog/reforma/trazos-pincel-catich-1200x536.png)<br>
• [MODULES > Capitales (Corentin Noyer, ‘Manipulation Typographique’)](https://anrt-nancy.fr/anrt-22/media/pages/projets/manipulation-typographique/b5232b4de5-1650381072/anrt-2015-2016-noyer-03.jpg)<br>
• [MODULES > bas-de-casse (Studio Triple, ‘Modules et anatomie’)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/01%20modules%20et%20anatomie.pdf)<br>
FONDERIES / SPÉCIMENS WEB<br>
• [Spécimens typographiques web](https://typespecimens.xyz/specimens/all/)<br>
• [Commercial Classics Showcase](https://showcase.commercialclassics.com/)<br>
• [OHno Type Co.](https://ohnotype.co/fonts)<br>
• [General Type Studio](https://www.generaltypestudio.com/)<br>
• [The Pyte Foundry](https://thepytefoundry.net/)<br>
CARACTÈRES<br>
• [Electric](https://www.myfonts.com/fonts/typodermic/electric/?refby=fiu)<br>
• [Elektrix](https://www.emigre.com/Fonts/Elektrix)<br>
• [Gemini](https://www.flickr.com/photos/stewf/16437312062/)<br>
• [GlyphWorld](https://femme-type.com/a-typeface-of-nine-landscapes-glyph-world/)<br>
• [Grassy](https://www.fontshop.com/families/linotype-grassy)<br>
• [Kaeru Kaeru](http://velvetyne.fr/fonts/kaeru-kaeru/)<br>
• [Pilowlava](http://velvetyne.fr/fonts/pilowlava/)<br>
• [Sea Weed](https://www.flickr.com/photos/hardwig/46965181465/in/photostream/)<br>
• [Wind](https://www.typotheque.com/fonts/wind)<br>
